# Basic

## Create the vitual environment

```bash
python3 -m venv venv

ln -s venv/bin/activate ./activate #mac and linux

```

## Activate the virtual environment

```bash
source ./activate
```

## Install the requirements

```bash
pip install -r requirements.txt
```

## Install the pre-commit hooks

```bash
pre-commit install
```

## Deactivate the virtual environment

```bash
deactivate
```
